# Project Documentation

## Project Setup
- Clone project
- Install composer
```
composer install
```
- Copy .env.example file to .env file
```
DB_DATABASE=testproject
DB_USERNAME=root
DB_PASSWORD=
```
- Generate key
```
php artisan key:generate
```
- Export database & populate default data
```
sql/sql.sql
```

- Running project
```
php artisan serve
http://127.0.0.1:8000
```

## Credential

- Admin
```
http://127.0.0.1:8000/bakpau982/me/login
username: admin
pass: 123456
```

- wartawan
```
http://127.0.0.1:8000/bakpau982/me/login
username: aryani
pass: 123456
```

Enjoy